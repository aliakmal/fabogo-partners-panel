let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([ 'node_modules/jquery/dist/jquery.min.js',
              'node_modules/jquery-ui-dist/jquery-ui.min.js',
              'node_modules/bootstrap/dist/js/bootstrap.min.js',

              'node_modules/bootstrap-daterangepicker/daterangepicker.js',
              'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
              'node_modules/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js',
              'node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
              'node_modules/timepicker/jquery.timepicker.min.js',

              'node_modules/raphael/raphael.js',              
              'node_modules/morris.js/morris.js',              
              'resources/assets/js/app.js'], 'public/js/app.js');

mix.combine([ 'node_modules/bootstrap/dist/css/bootstrap.min.css',
              'node_modules/font-awesome/css/font-awesome.min.css',
              
              'node_modules/jquery-ui-dist/jquery-ui.min.css',
              'node_modules/bootstrap-daterangepicker/daterangepicker.css',
              'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',

              'node_modules/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.min.css',
              'node_modules/bootstrap-select/dist/css/bootstrap-select.min.css',

              'node_modules/ionicons/dist/css/ionicons-core.css',
              'node_modules/ionicons/dist/css/ionicons.css',
              'node_modules/morris.js/morris.css',
              'node_modules/admin-lte/dist/css/AdminLTE.min.css',
              'node_modules/admin-lte/dist/css/skins/skin-purple-light.min.css',
              'node_modules/admin-lte/dist/css/skins/skin-blue-light.min.css',
              'node_modules/admin-lte/dist/css/skins/skin-yellow-light.min.css',
              'node_modules/admin-lte/dist/css/skins/skin-black-light.min.css',
              'node_modules/timepicker/jquery.timepicker.min.css',

              'resources/assets/css/app.css'], 'public/css/app.css');

mix.copy(['node_modules/bootstrap/dist/fonts/*',
          'node_modules/font-awesome/fonts/*',
          'node_modules/ionicons/dist/fonts/*'], 'public/fonts/');

mix.copy(['node_modules/bootstrap-select/dist/js/bootstrap-select.js.map'], 'public/js/');
mix.copy(['node_modules/ionicons/dist/css/*'], 'public/css/');
              


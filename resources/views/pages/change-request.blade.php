@extends('layouts.client')
@section('content')

<div class="row">
  <div class="col-md-8">
    <h1>Change Request</h1>
    <p>Need to make a change to something on your profile? Send in the details and we would follow up within 48 hours.</p>
    @include('elements.notifications')
    
    {{ BootForm::open()->post()->action('/partner/change-request')->encodingType('multipart/form-data') }}
      @if (Session::get('error'))
        <div class="alert alert-error alert-danger">
          {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
        </div>
      @endif
      @if (Session::get('notice'))
        <div class="alert alert-success">
          {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
        </div>
      @endif
      {{ BootForm::textarea('Details ', 'message')->placeholder('Enter as much detail as you want for change. If you require to upload special images, a representative from Fabogo would contact you')->required() }}
      <div class="form-group">
        {{ Form::submit('Send Request', array('class' => 'btn  btn-primary')) }}
      </div>
    {{ BootForm::close() }}
  </div>
  <div class="col-md-4">
    <blockquote>
      Change requests are mostly followed up within 48 hours or less. If you wish to upload any images, please mention it in your change request. A support representative from Fabogo would get in touch with you shortly.
    </blockquote>
  </div>
</div>
@stop
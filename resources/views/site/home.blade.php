@extends('layouts.no-sidebar')
@section('content')
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="callout callout-warning">
      <h4>Work in Progress!</h4>
      <p>Stay tuned - modules are being added by the hour.</p>
    </div>
  </div>
</div>
<div class="row">
  @foreach($dashboards as $dashboard)
    <div class="col-md-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua">
          @if($dashboard == 'sms')
            <i class="fa fa-envelope-o"></i>
          @elseif($dashboard == 'crm')
            <i class="fa fa-bullhorn"></i>
          @elseif($dashboard == 'content')
            <i class="fa fa-building-o"></i>
          @endif
        </span>
        <div class="info-box-content">
          <span class="info-box-text">DASHBOARD FOR</span>
          <span class="info-box-number">{{ strtoupper($dashboard) }}</span>
          <a href="/{{ $dashboard }}" class="info-box-footer">Open Dashboard <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  @endforeach
</div>
</section>
@endsection


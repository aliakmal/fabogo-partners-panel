<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
<link rel="apple-touch-icon" sizes="57x57" href="{{mzk_assets('assets/clients-favicons/apple-icon-57x57.png?v=2')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{mzk_assets('assets/clients-favicons/apple-icon-60x60.png?v=2')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{mzk_assets('assets/clients-favicons/apple-icon-72x72.png?v=2')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{mzk_assets('assets/clients-favicons/apple-icon-76x76.png?v=2')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{mzk_assets('assets/clients-favicons/apple-icon-114x114.png?v=2')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{mzk_assets('assets/clients-favicons/apple-icon-120x120.png?v=2')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{mzk_assets('assets/clients-favicons/apple-icon-144x144.png?v=2')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{mzk_assets('assets/clients-favicons/apple-icon-152x152.png?v=2')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{mzk_assets('assets/clients-favicons/apple-icon-180x180.png?v=2')}}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{mzk_assets('assets/clients-favicons/android-icon-192x192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{mzk_assets('assets/clients-favicons/favicon-32x32.png?v=2')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{mzk_assets('assets/clients-favicons/favicon-96x96.png?v=2')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{mzk_assets('assets/clients-favicons/favicon-16x16.png?v=2')}}">
<link rel="manifest" href="{{mzk_assets('assets/clients-favicons/manifest.json')}}">
<link rel="canonical" href="{{ Request::url() }}" />
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ mzk_assets('assets/clients-favicons/ms-icon-144x144.png?v=2') }}">

  <title>Fabogo Partner</title>
  <link type="text/css" rel="stylesheet" href="https://s3.amazonaws.com/mazkaracdn/css/client.min.css?v=12.01">
  <script src="https://s3.amazonaws.com/mazkaracdn/js/client.min.js?v=9.03"></script>



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
@if(in_array(env('APP_ENV'), ['production']))
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N4LM5P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!-- Google Tag Manager  
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N4LM5P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N4LM5P');</script>
 End Google Tag Manager -->
@elseif(in_array(env('APP_ENV'), ['demo']))
<!--<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PW4864"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PW4864');</script>
 -->
@endif
<!-- Google Tag Manager 

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PW4864"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PW4864');</script>
 End Google Tag Manager -->
  <div id="wrapper" style="padding-bottom:0px;">
<header role="banner" class="navbar">
  <div class="container">
    <div class="navbar-header">
      <button data-target=".navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
        <span class="sr-only">Toggle navigation</span>
        <i class="fa fa-cog"></i>
      </button>
      <a class="navbar-brand navbar-brand-img" style="padding-top:5px;" href="/">
        <img src="{{mzk_assets('assets/clients-logo-fabogo.png')}}" />
      </a>
    </div> <!-- /.navbar-header -->
    <nav role="navigation" class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-left">
        <!-- /.dropdown -->
        <li class="dropdown">
          <?php $user = \Auth::user();?>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Manage - {{ mzk_client_get_default_business_name() }} <i class="fa fa-caret-down"></i> 
          </a>
          <?php 
          $user = \App\Models\User::find($user->id);
          if($user->hasTheRoles(['admin', 'sales-admin', 'sales'])){
            $businesses = \App\Models\Business::whereIn('id', [mzk_client_bid()])->get();

          }else{
            $merchants = \DB::table('api_merchantuser')
                                  ->where('user_id', '=', $user->id)
                                  ->where('role', '=', 'merchant')
                                  ->pluck('merchant_id','merchant_id')->all();

            $businesses = \App\Models\Business::whereIn('merchant_id', $merchants)->get();

          }
          ?>
          <ul class="dropdown-menu dropdown-user">
            @foreach($businesses as $business)
              <li>
                <a href="/{{$business->id}}">{{$business->name}}, {{$business->zone_cache}}</a>
              </li>
            @endforeach
          </ul>
          <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <!-- /.dropdown -->
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <!-- <li><a href="/"><i class="fa fa-user fa-fw"></i> Back to Fabogo</a></li> 
            <li class="divider"></li>-->
            <li><a href="/users/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
          </ul>
          <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
      </ul>
    </nav>
  </div> <!-- /.container -->
</header>
<div class="mainnav">
  <div class="container">
    <a data-target=".mainnav-collapse" data-toggle="collapse" class="mainnav-toggle">
      <span class="sr-only">Toggle navigation</span>
      <i class="fa fa-bars"></i>
    </a>
    <nav role="navigation" class="collapse mainnav-collapse">
      <ul class="mainnav-menu">
        <li class="dropdown @if(Route::is('client.home') ) active @endif ">
          <a href="/{{ mzk_client_bid() }}" > 
            <i class="fa fa-dashboard fa-fw"></i> Analytics 
          </a>
        </li>
        <li class="dropdown  {{ strstr(Request::url(), '/reviews') ? 'active' : '' }}">
          <a href="/{{ mzk_client_bid() }}/reviews" ><i class="fa fa-comments fa-fw"></i> Manage Review(s)</a>
        </li>
        <li class="dropdown   {{ strstr(Request::url(), '/call_logs') ? 'active' : '' }}">
          <a href="/{{ mzk_client_bid() }}/call_logs" ><i class="fa fa-phone fa-fw"></i> Call Logs</a>
        </li>
        
        <li class="dropdown   {{ strstr(Request::url(), '/leads') ? 'active' : '' }}">
          <a href="/{{ mzk_client_bid() }}/leads" ><i class="fa fa-group fa-fw"></i> FabREACH</a>
        </li><!---->
        <li class="dropdown   {{ strstr(Request::url(), '/resumes') ? 'active' : '' }}">
          <a href="/{{ mzk_client_bid() }}/resumes" ><i class="fa fa-file fa-fw"></i> FabHIRE</a>
        </li>
        
      </ul>
    </nav>
  </div> <!-- /.container -->
</div>
<div class="container ">
  <div class="alert alert-warning">Dear valued customer - as part of our commitment to bring you better services, 
    we are undergoing a routine upgrade. 
    During this time certain services may be partially unavailable and the accuracy of analytics and other data cannot be guaranteed 100%
    from the partners panel. 

    We would be done with the upgrade by 14:00 UTC 6th February. 2018. 

    Any inconvenience is highly regretted, thank you for your cooperation - Team Fabogo.</div>
</div>
    <div id="page-wrapper" class=" container">
      @yield('content')
    </div>
    <!-- /#page-wrapper -->
  </div>
<footer class="footer">
  <div class="container">
    <p class="pull-left"> 
      
        Copyright &copy; 2018 
        <strong>Made with <i class="fa fa-heart text-danger"></i></strong>
        by Team Mazkara. All rights reserved.
  </p>
  </div>
</footer>  
  <!-- /#wrapper -->
  <script type="text/javascript">
    $(function(){
  $('.submit-parent-form').click(function(){
    $(this).parents().first().submit();
  });

  $('form.confirmable').submit(function(){
    return confirm('Are you sure?');  
  });

    $('.ajax-popup-link').magnificPopup({
      type: 'ajax',
      showCloseBtn:true,
      closeBtnInside:false,
      fixedContentPos: 'auto',
      closeOnBgClick:true,
    });    


      $(function(){
          $('input[type="date"]').datepicker({ format: "yyyy-mm-dd", autoclose:true });
      })
    });
  </script>
</body>

</html>

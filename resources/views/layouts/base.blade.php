<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fabogo Partners | 2.0.0</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/css/app.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <script src="/js/app.js"></script>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue-light  layout-top-nav">
<div class="wrapper">
  <header class="main-header">
    <nav style="background-color:#2b3d4c !important;" class="navbar  navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="/" class="navbar-brand" style="padding:5px 15px;"><img src="https://s3.amazonaws.com/mazkaracdn/assets/clients-logo-fabogo.png" /></a>
        </div>
        <div class="navbar-custom-menu navbar-left">
          <ul class="nav navbar-nav">
            <li class="dropdown tasks-menu">
              <?php $user = \Auth::user();?>
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                Manage - {{ mzk_client_get_default_business_name() }} <i class="fa fa-caret-down"></i> 
              </a>
              <?php 
              $user = \App\Models\User::find($user->id);
              if($user->hasTheRoles(['admin', 'sales-admin', 'sales'])){
                $businesses = \App\Models\Business::whereIn('id', [mzk_client_bid()])->get();

              }else{
                $merchants = \DB::table('api_merchantuser')
                                      ->where('user_id', '=', $user->id)
                                      ->where('role', '=', 'merchant')
                                      ->pluck('merchant_id','merchant_id')->all();

                $businesses = \App\Models\Business::whereIn('merchant_id', $merchants)->get();
              }
              ?>
              <ul class="dropdown-menu dropdown-user">
                @foreach($businesses as $business)
                  <li>
                    <a href="/{{$business->id}}">{{$business->name}}, {{$business->zone_cache}}</a>
                  </li>
                @endforeach
              </ul>
            </li>
          </ul>
        </div>
        <div class="navbar-custom-menu navbar-right">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <img src="https://www.gravatar.com/avatar/{{md5(mzk_get_signed_in_user_attrib('email'))}}" class="user-image" alt="User Image">
                <span class="hidden-xs">{{ mzk_get_signed_in_user_attrib('name') }}</span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header">
                  <img src="https://www.gravatar.com/avatar/{{md5(mzk_get_signed_in_user_attrib('email'))}}" class="img-circle" alt="User Image" />
                  <p>
                    {{ mzk_get_signed_in_user_attrib('name') }}
                    <small>Member since {{ mzk_get_signed_in_user_attrib('created_at') }}</small>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-right">
                    <a href="{{ route('users.logout') }}" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <div class="skin-black-light" >
    <header class="main-header " style="z-index:auto;">


<div class="mainnav container">
    <nav role="navigation" class="">
      <ul class="mainnav-menu nav navbar-nav">

        <li class="dropdown @if(Route::is('client.home') ) active @endif ">
          <a href="/{{ mzk_client_bid() }}" > 
            <i class="fa fa-dashboard fa-fw"></i> Analytics 
          </a>
        </li>
        <li class="dropdown  {{ strstr(Request::url(), '/reviews') ? 'active' : '' }}">
          <a href="/{{ mzk_client_bid() }}/reviews" ><i class="fa fa-comments fa-fw"></i> Manage Review(s)</a>
        </li>
        <li class="dropdown   {{ strstr(Request::url(), '/call_logs') ? 'active' : '' }}">
          <a href="/{{ mzk_client_bid() }}/call_logs" ><i class="fa fa-phone fa-fw"></i> Call Logs</a>
        </li>
        
        <li class="dropdown   {{ strstr(Request::url(), '/leads') ? 'active' : '' }}">
          <a href="/{{ mzk_client_bid() }}/leads" ><i class="fa fa-group fa-fw"></i> FabREACH</a>
        </li><!---->
        <li class="dropdown   {{ strstr(Request::url(), '/resumes') ? 'active' : '' }}">
          <a href="/{{ mzk_client_bid() }}/resumes" ><i class="fa fa-file fa-fw"></i> FabHIRE</a>
        </li>
        
      </ul>


      </nav>
    </header>
  </div>

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      @yield('content')
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 6.0
      </div>
      <strong>
        Copyright &copy; <?php echo date('Y');?> 
        Made with <i class="fa fa-heart text-danger"></i>
        by Team Mazkara. All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

</body>
</html>

@extends('layouts.partners')
@section('content')
<div class="content">

    <div class="container">



<div class="row">
  <div class="col-md-9">
      <h3 class="portlet-title">
FabREACH</h3>

<?php $timezone = $business->isUAE()?'uae':'ist';?>
<?php $current_date = false; ?>
@if ($leads->count())
  <table class="table table-striped">
  @foreach($leads as $lead)
    @if(\Carbon\Carbon::parse($lead->created_at)->toFormattedDateString() != $current_date)
      <?php $current_date = \Carbon\Carbon::parse($lead->created_at)->toFormattedDateString();?>
          <tr><td style="background-color: white; border:0px;" colspan="5">
      <h4>{{ $current_date }}</h4>
    </td></tr>
          <tr>
            <th width="20%">Name</th>
            <th width="20%">Phone </th>
            <th width="25%">Email</th>
            <th width="25%">Interested In</th>
            <th></th>
          </tr>
    @endif
    <tr>
      <td>{{ $lead->name }}</td>
      <td>{{ $lead->phone_number }} </td>
      <td>{{ $lead->email }}</td>
      <td>{{ $lead->interested_in }}</td>
      <td></td>
    </tr>
  @endforeach
  </table>



  <div class="row">
    <div class="col-md-12">
  {{ $leads->appends($params)->render() }}
  <div class="pull-right">
    {{ count($leads) }} / {{ $leads->total() }} entries
  </div></div>
</div>

@else

  There are no leads
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
@endif
</div>
<div class="col-md-3">

  <div class="portlet portlet-boxed">
    <div class="portlet-header">
      <h4>Filter Leads</h4>
    </div>
    <div class="portlet-body">
      <form >
        <!--<div class="input-daterange  " id="datepicker">
          <input type="text" placeholder="Select Start Date" value="{{ isset($params['start'])?$params['start']:'' }}" class="input-sm form-control" id="start" name="start" />
          <br/>
          <input type="text" placeholder="Select End Date" value="{{ isset($params['end'])?$params['end']:'' }}" class="input-sm form-control" id="end" name="end" />
        </div>
        <br/> -->
        <input type="text" placeholder="Search?" value="{{ isset($params['search'])?$params['search']:''}}" class="input-sm form-control" id="search" name="search" />
          <br/>
        <input type="text" placeholder="Interested In?" value="{{ isset($params['interested'])?$params['interested']:''}}" class="input-sm form-control" id="interested" name="interested" />
          <br/>
        <p>
          <input type="submit" class="btn-block btn btn btn-primary" value="Apply" />
          <a href="leads" style="margin-top:5px;" class="btn btn-sm btn-block btn-default">Reset</a>
        </p>
      </form>
    </div>
  </div>


</div>
    </div> <!-- /.container -->

  </div>
</div>

<script type="text/javascript">
$(function(){
    $('.input-daterange').datepicker({
      format: "yyyy-mm-dd", autoclose:true
  });    


});
</script>
@stop
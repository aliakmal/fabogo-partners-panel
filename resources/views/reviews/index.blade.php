@extends('layouts.partners')
@section('content')

<div class="portlet portlet-default">
  <div class="portlet-header">
    <h3 class="portlet-title">
      Reviews for {{$business->name}}
    </h3>
</div>
</div>
<div class="row">
  <div class="col-md-8">
    @if (count($reviews)>0)
			@foreach ($reviews as $review)
        <h4>
          {{{ $review->user->full_name }}} 
          <span title="Rated {{ $review->rating }} stars">
            @for($i=0;$i<$review->rating;$i++)
              <i class="fa fa-star"></i>
            @endfor
          </span>

        </h4>
        <p>{{{ $review->body }}}</p>
        <div>
          <span class="badge">Posted  {{{ \Carbon\Carbon::parse($review->updated_at)->diffForHumans() }}}</span>
          <div class="pull-right hidden">
            <a class="btn btn-xs btn-primary" href="/partner/{{mzk_client_bid()}}/reviews/{{$review->id}}">Respond as Management</a>
          </div>         
        </div>
        <hr>
			@endforeach
      <div class="row">
        <div class="col-md-12">
          {{ $reviews->render() }}
          <div class="pull-right">
            {{ count($reviews) }} / {{ $reviews->total() }} entries
          </div>
        </div>
      </div>
      <p>&nbsp;&nbsp;</p>
    @else
    	There are no reviews
    @endif
  </div>
  <div class="col-md-4">
    <div class="row">
      <div class="col-md-6 text-center">
        <div class="row-stat">
          <h3>{{$total_reviews_count}}</h3>
          <small class="text-muted">Total Reviews</small>
        </div>      
      </div>
      <div class="col-md-6 text-center">
        <div class="row-stat">
          <h3>{{count($responded_reviews)}}</h3>
          <small class="text-muted">Reviews responded to</small>
        </div>      
      </div>
    </div>
  </div>
</div>
@stop
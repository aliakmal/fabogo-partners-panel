@extends('layouts.client')
@section('content')


    <h4>{{{ $review->user->full_name }}} {{ ViewHelper::starRateSmallBasic($review)}}</h4>
    <p>{{{ $review->body }}}</p>
    <div>
<span class="badge">Posted  {{{ Date::parse($review->updated_at)->ago() }}}</span>
<div class="pull-right">
  @if(count($review->comments)>0)
    <span class="btn btn-xs btn-primary">{{count($review->comments)}} Comments</span> 
  @else
    <span class="btn btn-xs btn-default">0 Comments</span> 
  @endif
  <a class="btn btn-xs btn-primary" href="/partner/{{mzk_client_bid()}}/reviews/{{$review->id}}">view</a>
  </div>         
 </div>
<hr>

  @if(count($review->comments)>0)
    <div class="mt10 pt10 fs90">
      @foreach($review->comments as $comment)
      <div class="media pb10 pt10 bb">
          @if(Auth::check())
            @if($comment->isDeletableBy(Auth::user()))
              {{ Form::open(array('style' => 'display: inline-block;', 
                                  'class'=>'pull-right confirmable',  'method' => 'DELETE', 
                                  'route' => array('comments.destroy', $comment->id))) }}
                <a href="javascript:void(0)" class="submit-parent-form"><i class="fa fa-times"></i></a>

              {{ Form::close() }}
            @endif

          @endif
        <div class="media-left">
          <a href="/users/{{$comment->user->id}}/profile" >
            {{ViewHelper::userAvatar($comment->user, ViewHelper::$avatar35)}}

          </a>
        </div>
        <div class="media-body dark-gray">
          <b>{{$comment->getDisplayableUsersName()}}</b> <span class="medium-gray">{{$comment->body}}</span>

          <p><small class="gray">
            
              {{{ Date::parse($comment->updated_at)->ago() }}}
          </small></p>

        </div>
      </div>

      @endforeach
    </div>
  @endif
  <?php $usr = Auth::user();?>
  @if($usr)
    <div class="media pb10 pt10 ">
  <div class="media-left">
    <a href="/users/{{$usr->id}}/profile" >
      {{ViewHelper::userAvatar($usr, ViewHelper::$avatar35)}}
    </a>
  </div>
  <div class="media-body" style="width: 100%;">

    {{ Form::open(array('route' => 'comments.store')) }}
      {{ Form::hidden('commentable_type', 'Review')}}
      {{ Form::hidden('commentable_id', $review->id)}}
      {{ Form::hidden('type', 'comment')}}
      {{ Form::text('body', '', ['class'=>'form-control count-limiter', 'placeholder'=>'Write a comment'])}}
    {{ Form::close()}}
  </div>
</div>



@endif
@stop

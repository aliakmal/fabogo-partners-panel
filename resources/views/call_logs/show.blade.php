@extends('layouts.client')
@section('content')

<h1>Show Call log</h1>



	<table class="table table-striped">
		<thead>
			<tr>
				<th>Date</th>
				<th>Time</th>
				<th>Caller </th>
			</tr>
		</thead>

		<tbody>
				<tr>
					<td>{{{ $call_log->dated }}}</td>
					<td>{{{ $call_log->timed }}}</td>
					<td>{{{ $call_log->caller_number }}}</td>
				</tr>
			</tbody>
		</table>
	<div>
	  @if(strlen($call_log->media_url))
	     <audio controls>
	    <source src="{{ $call_log->media_url }}" type="audio/ogg">
	    <source src="{{ $call_log->media_url }}" type="audio/mpeg">
	  Your browser does not support the audio element.
	    </audio>
	    @endif
	</div>

  @if($call_log->isPotentialLead())
    <i class="fa fa-money" title="This call is a potential lead and is billable."></i>
  @endif
  {{ mzk_call_logs_state_helper($call_log->state) }}
  @if($call_log->isDisputable())
    <a href="javascript:void(0)" class="btn lnk-to-dispute btn-default btn-lg" data-call="{{ $call_log->id }}">DISPUTE</a>
  @endif
  @if($call_log->canBeResolvedByUser())
    @if($call_log->isResolvable())
      <a href="javascript:void(0)" class="btn lnk-to-resolve btn-default btn-lg" data-call="{{ $call_log->id }}">RESOLVE</a>
    @endif
  @endif

@stop

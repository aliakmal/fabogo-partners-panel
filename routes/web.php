<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {

  Route::get('/client/login', ['uses'=>'SiteController@login']);

Route::get('/redirect/client/dashboard', 'HomeController@redirectToClientDashboard');
Route::group(['middleware'=>['restrict.access']], function(){

  Route::get('/{bid}', ['as'=>'client.home', 'uses'=>'HomeController@dashboard']);
  Route::get('/', 'SiteController@clientHomeRedirector');
  Route::get('/set-default-business/{id}', 'SiteController@setCurrentBusiness');
    Route::get('/{bid}/reviews', ['as'=>'client.reviews.index', 'uses'=>'ReviewsController@index']);
    Route::get('/{bid}/reviews/{id}', ['as'=>'client.reviews.show', 'uses'=>'ReviewsController@show']);
    //  Route::get('/{bid}/photos', ['as'=>'client.businesses.show.photos', 
    //                          'uses'=>'ProfileController@getPhotos']);
    //  Route::post('/{bid}/photos', 'ProfileController@postPhotos');
    Route::get('/{bid}/leads', 'LeadsController@index');
    Route::get('/{bid}/resumes', 'ResumesController@index');
    //  Route::get('/{bid}/rate-cards', [ 'as'=>'client.businesses.show.rate_cards', 
    //                              'uses'=>'ProfileController@getRateCards']);
    //  Route::post('/{bid}/rate-cards', 'ProfileController@postRateCards');
    //  Route::get('/{bid}/validate/voucher/form', [ 'as'=>'client.voucher.validate.form', 
    //                              'uses'=>'OffersController@validateVoucherForm']);
    //  Route::post('/{bid}/validate/voucher', [ 'as'=>'client.voucher.validate', 
    //                              'uses'=>'OffersController@postValidateVoucher']);
    //  Route::post('/{bid}/voucher/{id}/redeem', [ 'as'=>'client.voucher.redeem', 
    //                              'uses'=>'OffersController@redeemVoucher']);
    //
    //  Route::any('/{bid}/google', 'HomeController@google');
    //  Route::any('/{bid}/api/ratings', 'ReviewsController@getRatingsData');
    Route::any('/{bid}/api/views', 'HomeController@getViewsDataApi');
    Route::any('/{bid}/api/calls', 'HomeController@getCallsDataApi');
    Route::any('/{bid}/api/call_logs', 'HomeController@getCallLogsDataApi');
    //  Route::get('/{bid}/change-request', 'ProfileController@getChangeRequest');
    //  Route::post('/{bid}/change-request', [ 'as'=>'client.post.change-request', 
    //                              'uses'=>'ProfileController@postChangeRequest']);
    //  Route::group(array('prefix'=>'{bid}'), function(){
    //    Route::resource('offers', 'OffersController');
    //    Route::resource('offers', 'OffersController', array('names' => mzk_route_helper('offers', 
    //      'partner')));
    //  });
    //
    //
    //  Route::get('users/logout', 'client\\HomeController@logout');
    //  Route::get('unauthenticated', 'client\\HomeController@unauthenticated');
    //
      Route::group(array('prefix'=>'{bid}'), function(){
        Route::get('call_logs/dispute/{id}', ['uses'=>'Call_logsController@createDispute', 'as'=>'partner.call_logs.disputes.store']);
        Route::post('call_logs/dispute', ['uses'=>'Call_logsController@storeDispute', 'as'=>'partner.call_logs.disputes.create']);
    
        Route::get('call_logs/resolve/{id}', ['uses'=>'Call_logsController@createResolve', 'as'=>'partner.call_logs.resolves.store']);
        Route::post('call_logs/resolve', ['uses'=>'Call_logsController@storeResolve', 'as'=>'partner.call_logs.resolves.create']);
        Route::get('call_logs/reset/{id}', ['uses'=>'Call_logsController@resetCallLog', 'as'=>'partner.call_logs.reset']);
        Route::get('call_logs/billable/{id}', ['uses'=>'Call_logsController@clearToBilled', 'as'=>'partner.call_logs.clear.to.billed']);
    
        Route::group(array('before' => 'client.admin.auth'), function(){
          Route::get('call_logs/export', ['uses'=>'Call_logsController@export', 'as'=>'partner.call_logs.export']);
        });
    
        Route::resource('call_logs', 'Call_logsController');
        Route::resource('call_logs', 'Call_logsController', 
            array('names' => mzk_route_helper('call_logs', 
                  'partner')));
      });
    
    Route::get('users/logout', ['as'=>'users.logout', 'uses'=>'SiteController@logout']);
    //  Route::get('/{bid}/photo/{photoId}', ['as'=>'client.businesses.show.photo','uses'=>'client\\ProfileController@getPhoto']);
    //  Route::post('/{bid}/photo/{photoId}', ['as'=>'client.businesses.edit.photo', 'uses'=>'client\\ProfileController@postPhoto'] );
    //  Route::get('/{bid}/photo/{photoId}/rotate/{degrees}', ['as'=>'client.businesses.rotate.photo',
    //                                                'uses'=>'client\\ProfileController@rotatePhoto']);
  });

});

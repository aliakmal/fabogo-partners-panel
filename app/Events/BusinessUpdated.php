<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use \App\Models\Business;

class BusinessUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $business;

    public function __construct(\App\Models\Business $business)
    {
        $this->business = $business;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}

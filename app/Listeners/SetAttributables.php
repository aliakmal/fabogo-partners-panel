<?php

namespace App\Listeners;

use App\Events\BusinessUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetAttributables
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BusinessUpdated  $event
     * @return void
     */
    public function handle(BusinessUpdated $event)
    {
        $business = $event->business;
        $business->zone_cache = $business->zone->name;
        $business->city_name = $business->zone->city->name;
    }
}

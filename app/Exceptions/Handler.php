<?php

namespace App\Exceptions;

use Exception, Request;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Mail;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use App\Mail\ExceptionOccured;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($this->shouldReport($exception)) {
        $this->sendEmail($exception);
        return response()->view('site.404');
            //app('sneaker')->captureException($exception);
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $this->sendEmail($exception);

        return response()->view('site.404');

        return parent::render($request, $exception);
    }

    public function sendEmail(Exception $exception){
        try {
            $e = FlattenException::create($exception);
            $handler = new SymfonyExceptionHandler();
            $data = [];
            $data['content'] = $handler->getHtml($e);

            $data['url'] = Request::url();
            $data['input'] = Request::all();
            $data['url_full'] = $data['url'].(count($data['input']>0)?'?'.http_build_query($data['input']):'');

            Mail::to('ali@fabogo.com')->send(new ExceptionOccured($data));

        } catch (Exception $ex) {
            dd($ex);
        }
    }
}

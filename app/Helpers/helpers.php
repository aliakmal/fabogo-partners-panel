<?php

function mzk_get_cities_array($force = true){
  $label = 'mazkara.cities';

  if(\Session::has($label) && ($force != true)){
    return \Session::get($label);
  }else{
    mzk_set_cities_array();
    return \Session::get($label);
  }
}

function mzk_set_cities_array(){
  $label = 'mazkara.cities';
  \Session::put($label, \App\Models\City::select()->get());
}
function mzk_call_logs_state_helper($state){

  $html = '<span ';
  $class = '';
  switch($state){
    case \App\Models\Call_log::STATE_CLEAR:
      $class="label label-info";
    break;
    case \App\Models\Call_log::STATE_UNBILLED:
      $class="label label-warning";
    break;
    case \App\Models\Call_log::STATE_DISPUTED:
      $class="label label-danger";
    break;
    case \App\Models\Call_log::STATE_BILLED:
      $class="label label-success";
    break;
    case \App\Models\Call_log::STATE_RESOLVED:
      $class="label-primary";
    break;

  }
  $html.=' class="'.$class.' label">';
  $html.= strtoupper($state);
  $html.='</span>';

  return $html;

}



function mzk_get_default_city(){
  $label = 'mazkara.admin.locale';
  if(\Session::has($label)){
    return \Session::get($label);
  }else{
    $cities = mzk_get_cities_array();
    $city = $cities->first();

    mzk_set_default_city($city);
    return \Session::get($label);
  }
}

function mzk_set_default_city($city){
  $label = 'mazkara.admin.locale';
  \Session::put($label, $city);
}

function mzk_get_localeID(){

  return mzk_get_locale_id();
}

function mzk_get_locale_id(){
  $city = mzk_get_default_city();
  return $city->id;
}

function mzk_get_current_locale_attrib($attrib){
  $city =  mzk_get_default_city();
  if(isset($city->$attrib)){
    return $city->$attrib;
  }else{
    return false;
  }

  
}

function mzk_get_signed_in_user_attrib($attrib){
  $user = \Auth::user();

  if(isset($user->$attrib)){
    return $user->$attrib;
  }else{
    return false;
  }
}


function mzk_route_helper($resource, $prefix = false, $skip = false){
  $a = ['index','create','store','show','edit','update','destroy'];
  if(is_array($skip)){
    foreach($skip as $vv){
      $a = array_diff($a, [$vv]);
    }
  }
  $r = [];
  foreach($a as $v){
    $r[$v] = ($prefix ? $prefix.'.' : '').$resource.'.'.$v;
  }

  return $r;
}

function mzk_assets($file){
  $str = 'https://s3.amazonaws.com/mazkaracdn/';
  return $str.$file;
}

function mzk_client_set_default_business($business_id = false){
  $session = 'mazkara.clients.business';
  /*if(!Auth::user()->can('access_client_dashboards')):
    $users_businesses = Auth::user()->businesses();
    $ids = [];
    foreach($users_businesses as $business){
      $ids[] = $business->id;
    }


    if((!$business_id)){
      $business_id = current($ids);
    }
    if(!in_array($business_id, $ids)){
      $business_id = current($ids);
    }
  endif;*/


  Session::put($session, $business_id);
}

function mzk_client_get_default_business(){
  $session = 'mazkara.clients.business';

  if(!Session::has($session)){
    mzk_client_set_default_business();
  }
  return  Session::get($session);

}


function mzk_client_bid(){
  return Route::current()->parameter('bid');
}

function mzk_client_get_default_business_name(){
  $business = \App\Models\Business::find(mzk_client_bid());//mzk_client_get_default_business());
  return $business->name.', '.$business->zone_cache;
}
function mzk_client_get_default_business_object(){
  return \App\Models\Business::find(mzk_client_bid());//mzk_client_get_default_business());
}

function mzk_get_weeks_between_range_array($start, $end){
  $dates = range(strtotime($start), strtotime($end),604800);
  $weeks = [];
  $ws = array_map(function($v){ 
              $w = date('W', $v);
              $week_start = new DateTime();
              $week_start->setISODate(date('Y', $v), $w);
              return  $week_start->format('d-M-Y');
            }, $dates); // Requires PHP 5.3+
  $lastElementKey = key($ws);
  foreach($ws as $k=>$week):
    if( next( $ws ) ) {
      $weeks[] = [$week, $ws[$k+1]];
    }
  endforeach;

  return $weeks;
}

function mzk_get_countries_list(){
  return config('countries');
}

function mzk_get_country_from_list($code){
  $countries = mzk_get_countries_list();
  return isset($countries[$code])?$countries[$code]:$code;
}

function mzk_get_code_from_list($country){
  $countries = mzk_get_countries_list();
  return array_search($country, $countries);
}

function mzk_f_date($dt, $format = 'Y-m-d'){
  return date($format, strtotime($dt));
}

function mzk_time_convert_from_ist_to_utc($date_time){
  $dt = Carbon\Carbon::parse($date_time);
  $dt->subHours(5);
  $dt->subMinutes(30);
  return $dt;
}

function mzk_time_convert_from_ist_to_uae($date_time){
  $dt = mzk_time_convert_from_ist_to_utc($date_time);
  $dt->addHours(4);
  return $dt;
}

function mzk_current_datetime(){
  return Carbon\Carbon::now();
}

function mzk_current_date_time(){
  return Carbon\Carbon::now();
}

function mzk_create_month_range_array($start, $end){
  $start    = (new DateTime($start))->modify('first day of this month');
  $end      = (new DateTime($end))->modify('first day of next month');
  $interval = DateInterval::createFromDateString('1 month');
  $period   = new DatePeriod($start, $interval, $end);
  $range = [];

  foreach ($period as $dt) {
    $range[] = $dt->format("Y-m");
  }

  return $range;
}

function mzk_create_date_range_array($strDateFrom,$strDateTo)
{
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

$start    = new DateTime($strDateFrom);
$end      = new DateTime($strDateTo);
$interval = DateInterval::createFromDateString('1 day');
$period   = new DatePeriod($start, $interval, $end);
return $period;

    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    return $aryRange;
}

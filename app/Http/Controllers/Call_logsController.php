<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth, DB, URL, View, Redirect, Response;


class Call_logsController extends Controller {

	/**
	 * Call_log Repository
	 *
	 * @var Call_log
	 */
	protected $call_log, $business;

	public function __construct(\App\Models\Call_log $call_log, \App\Models\Business $business)
	{
		$this->call_log = $call_log;
		$this->business = $business;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($bid)
	{
		$current_business_id = $bid;//mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);

		$user = \App\Models\User::find(\Auth::user()->id);

		if(!$user->hasTheRoles(['admin', 'sales', 'sales-admin'])){
			$merchant_ids = \DB::table('api_merchantuser')->where('user_id', '=', $user->id)->pluck('merchant_id','merchant_id')->all();
			$business_ids = \App\Models\Business::whereIn('merchant_id',  $merchant_ids)->pluck('id','id')->all();
			if(!isset($business_ids[$bid])){
	    	return redirect('/');
			}
		}

		$input = Input::all();
		$start_date = isset($input['start']) ? $input['start'] : \Carbon\Carbon::now()->subDays(30)->toDateString();
		$end_date  = isset($input['end']) ? $input['end'] :  \Carbon\Carbon::now()->toDateString();
		$params = ['start'=>$start_date, 'end'=>$end_date];

		$call_logs = $this->call_log->query()->accessibleByClient();
		$call_logs = $call_logs->byBusiness($business->id)->betweenDates($start_date, $end_date)->orderBy('api_calllog.dated', 'DESC')->orderby('api_calllog.timed', 'DEC');
		$call_logs = $call_logs->paginate(20);

		return View::make('call_logs.index', compact('call_logs', 'params', 'start_date', 'end_date', 'business'));
	}


	public function createDispute($bid, $id)
	{
    return view('call_logs.disputes.create', compact('id'));
	}

	public function storeDispute(){
		$input = Input::all();
		$call_log = \App\Models\Call_log::find($input['call_log_id']);
		$call_log->dispute();

		$data = ['body'=>$input['description'], 
						 'commentable_type'=>'Call_log', 
						 'commentable_id'=>$call_log->id,
						 'type'=>Comment::CALL_LOG_DISPUTE];

		Comment::create($data);

		return redirect()->back()
			->with('notice', 'Dispute has been logged.');
	}


	public function createResolve($bid, $id)
	{
    return view('call_logs.resolves.create', compact('id'));
	}

	public function storeResolve(){
		$input = Input::all();
		$call_log = \App\Models\Call_log::find($input['call_log_id']);
		if($input['action'] == '0'){ // is not a lead
			$call_log->resetLead();

		}else{
			$call_log->resolve();
		}

		$data = ['body'=>$input['description'], 
						 'commentable_type'=>'Call_log', 
						 'commentable_id'=>$call_log->id,
						 'type'=>Comment::CALL_LOG_FOLLOWUP];

		Comment::create($data);

		return redirect()->back()
			->with('notice', 'Dispute has been Resolved.');
	}

	public function resetCallLog($bid, $id){
		$call_log = \App\Models\Call_log::find($id);
		$user = \App\Models\User::find(\Auth::user()->id);

		if($call_log->canBeResolvedByUser($user) && $call_log->isResetable()){
			$call_log->resetLead();
		}

		return redirect()->back();

	}


	public function clearToBilled($bid, $id){
		$call_log = \App\Models\Call_log::find($id);
		$user = \App\Models\User::find(\Auth::user()->id);

		if($call_log->canBeResolvedByUser($user) && $call_log->isClear()){
			$call_log->makeClearToBillable();
		}

		return redirect()->back();
	}

	public function export($bid){
		global $bzness;
		$bzness = $this->business->find($bid);
    $nm = 'call-logs-'.$bzness->slug.'-'.time();
    
		Excel::create($nm, function($excel) {

	    $excel->sheet('Calls', function($sheet) {

				$columns = ['caller_number', 'date'];
				$data = [];
				global $bzness;
				$call_logs = $this->call_log->byBusiness($bzness->id)->orderBy('api_calllog.id', 'DESC')->get()->toArray();
				foreach($call_logs as $call_log){
					$row = array();
					$row['caller_number'] = $call_log['caller_number'];
					$row['date'] = $call_log['dated'].' '.$call_log['timed'];
					$data[] = $row;
				}
	      $sheet->fromArray($data);
		  });

		})->export('csv');

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('call_logs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, \App\Models\Call_log::$rules);

		if ($validation->passes()){
			$this->call_log->create($input);
			return redirect()->route('call_logs.index');
		}

		return Redirect::route('call_logs.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$call_log = $this->call_log->findOrFail($id);

		return view('call_logs.show', compact('call_log'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$call_log = $this->call_log->find($id);

		if (is_null($call_log))
		{
			return Redirect::route('partner.call_logs.index');
		}

		return View::make('client.call_logs.edit', compact('call_log'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Call_log::$rules);

		if ($validation->passes())
		{
			$call_log = $this->call_log->find($id);
			$call_log->update($input);

			return Redirect::route('partner.call_logs.show', $id);
		}

		return Redirect::route('partner.call_logs.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->call_log->find($id)->delete();

		return Redirect::route('partner.call_logs.index');
	}

}

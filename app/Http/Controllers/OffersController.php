<?php
namespace App\Http\Controllers\client;

use Confide, URL, Auth, View, Config, Validator, Redirect, Input, Mail, Response;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Voucher;
use App\Models\Offer;
use App\Models\Business;
use App\Models\Menu_group;

class OffersController extends Controller {

	/**
	 * Offer Repository
	 *
	 * @var Offer
	 */
	protected $offer, $business;

	public function __construct(Offer $offer, Business $business)
	{
		$this->offer = $offer;
		$this->business = $business;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($bid = false)
	{
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);

    if(!$business->canCurrentUserAccessClientDashboard(Auth::user())){
    	return Redirect::to('/');
    }


		$offers = $this->offer->query();
		$offers = $offers->byBusiness($business->id)->paginate(20);

		$active_offers = $this->offer->query()->byBusiness($business->id)->byState('active')->count();
		$inactive_offers = $this->offer->query()->byBusiness($business->id)->byStates(['pending', 'inactive'])->count();

		return  View::make('client.offers.index', compact('offers', 'business', 'active_offers', 'inactive_offers'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($bid)
	{
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);


    if(!$business->canCurrentUserAccessClientDashboard(Auth::user())){
    	return Redirect::to('/');
    }


		return View::make('client.offers.create', compact('business'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($bid)
	{
		$input = Input::all();
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();

		$input['business_id'] = $current_business_id;
//		$input['state'] = 'active';
		$fields = Offer::$fields;
		$data = Input::only($fields);
		$data['business_id'] = $current_business_id;
		$service_items = Input::only('service_items');
		$service_items = count($service_items['service_items'])>0?$service_items['service_items']:[];
		$data['discount_amount'] = $data['discount_type'] == 'percentage' ? $input['percentage_amount']:$input['amount_off'];

		$services = Input::only('services');
		$services = count($services['services'])>0?$services['services']:[];

		$validation = Validator::make(Input::only($fields), Offer::$rules);
		$url = URL::previous();

		if ($validation->passes()){
			$data['state'] = 'active';
			$offer = $this->offer->create($data);
			$business = $offer->business;
			$offer->services()->sync($services);
			$offer->service_items()->sync($service_items);
			$offer->resetState();
			$business->updateMetaOffers();

			$input['business'] = $business;

      $deletablePhotos = Input::only('deletablePhotos');
      if($deletablePhotos){
        $offer->removeImage();
      }

      $images = Input::only('photo');
      $offer->saveImage($images['photo']);


//    Mail::send('emails.client-offers-added',['data'=>$input, 'server'=>$_SERVER], function($message) use ($input)
//    {
//        $message->to('moderation@mazkara.com');
//        $message->subject($input['business']->name.' has added an offer for moderation ['.time().']');
//        $message->from('info@mazkara.com');
//
//    });



			return Redirect::route('partner.offers.index', $bid);
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($bid, $id)
	{
		$offer = $this->offer->findOrFail($id);

		return View::make('client.offers.show', compact('offer'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($bid, $id)
	{
		$offer = $this->offer->find($id);
		$business = $offer->business;

    if(!$business->canCurrentUserAccessClientDashboard(Auth::user())){
    	return Redirect::to('/');
    }

		if (is_null($offer)){
			return Redirect::route('partner.offers.index');
		}

		return View::make('client.offers.edit', compact('offer', 'business'));
	}

	public function validateVoucherForm($bid){
		return View::make('client.offers.validate');
	}

	public function redeemVoucher($bid, $id){
		$data = Input::all();
		$voucher = Voucher::where('code', '=', $data['code'])->first();
		$voucher->claim();
		$result = [];

		$view = View::make('client.offers.redeemed', compact('voucher'));
		$result['html'] = $view->render();

		return Response::Json($result);

	}

	public function postValidateVoucher($bid){
		$data = Input::all();
		$voucher = Voucher::where('code', '=', $data['code'])->first();
		$result = [];
		if($voucher){
			if($voucher->offer->business_id == $bid){
				if($voucher->isClaimed()){
					$view = View::make('client.offers.claimed', compact('voucher'));
					$result['html'] = $view->render();
				}else{
					$view = View::make('client.offers.voucher-redeem-form', compact('voucher'));
					$result['html'] = $view->render(); 
				}
			}else{
				$view = View::make('client.offers.invalid');
				$result['html'] = $view->render();
			}
		}else{
			$view = View::make('client.offers.invalid');
			$result['html'] = $view->render();
		}
		return Response::Json($result);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($bid, $id)
	{
		$input = array_except(Input::all(), '_method');

		$validation = Validator::make($input, Offer::$rules);
		$fields = Offer::$fields;
		$data = Input::only($fields);
		$service_items = Input::only('service_items');
		$service_items = count($service_items['service_items'])>0?$service_items['service_items']:[];

		$services = Input::only('services');
		$services = count($services['services'])>0?$services['services']:[];
		$data['discount_amount'] = $data['discount_type'] == 'percentage' ? $input['percentage_amount']:$input['amount_off'];



		if ($validation->passes())
		{
			$offer = $this->offer->find($id);
			$data['business_id'] = $offer->business_id;
			$offer->update($data);
			$business = $offer->business;
			$offer->service_items()->sync($service_items);
			$offer->services()->sync($services);
			$offer->resetState();
			$business->updateMetaOffers();

			$input['business'] = $business;

      $deletablePhotos = Input::only('deletablePhotos');
      if($deletablePhotos){
        $offer->removeImage();
      }

      $images = Input::only('photo');
      $offer->saveImage($images['photo']);

			$input['business'] = $business;

//    Mail::send('emails.client-offers-added',['data'=>$input, 'server'=>$_SERVER], function($message) use ($input)
//    {
//        $message->to('moderation@mazkara.com');
//        $message->subject($input['business']->name.' has added an offer for moderation ['.time().']');
//        $message->from('info@mazkara.com');
//
//    });


			return Redirect::route('partner.offers.show', [$bid, $id]);
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($bid, $id)
	{
		$offer = $this->offer->find($id);

		$business = $offer->business;
		$offer->delete();
		$business->updateMetaOffers();

		$url = URL::previous();

		return Redirect::to($url);
	}

}

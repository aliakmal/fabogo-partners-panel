<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator, Analytics, Auth, Storage, Response;
use Spatie\Analytics\Period;
class HomeController extends Controller
{


  public function dashboard($bid = false){

    $user = \App\Models\User::find(Auth::user()->id);
    if($user->hasTheRoles(['admin', 'sales', 'sales-admin'])){
      $business_ids = [$bid];
    }else{
      $merchant_ids = \DB::table('api_merchantuser')
                            ->where('user_id', '=', $user->id)
                            ->pluck('merchant_id','merchant_id')->all();
      $business_ids = \App\Models\Business::whereIn('merchant_id',  $merchant_ids)->pluck('id','id')->all();

      if(!isset($business_ids[$bid])){
        return redirect('/');
      }
    }

    $businesses = \App\Models\Business::select()->whereIn('id', $business_ids)->get();

    if(count($businesses)==0){
        return redirect('/');
    }

    $current_business_id = $bid ? $bid : current($business_ids);
    $business = \App\Models\Business::find($current_business_id);
    $zone = \App\Models\Zone::find($business->zone_id);
    $city = \App\Models\City::find($zone->city_id);
    $start_date = \Carbon\Carbon::now()->subDays(30);
    $end_date  = \Carbon\Carbon::now();


    $slug = '/'.$city->slug.'/'.$business->slug;

    $count_page_views = 0;
    $count_avg_page_views = 0;
    $count_favorites = 0;
    $count_call_views = 0;

    $slug = $city->slug.'/'.$business->slug;

    $metrics='ga:pageviews,ga:goal1Completions';
    $opts = ['dimensions'    => 'ga:date',
                    'filters'       => 'ga:pagePath=@'.$slug];

    $analyticsData = Analytics::performQuery(Period::days(30), $metrics, $opts);


    $alt_calls = \App\Models\Call_log::selectRaw('count(id) as count_calls, dated')
                                                    ->accessibleByClient()
                                                    ->byBusiness($current_business_id)
                                                    ->betweenDates($start_date, $end_date)
                                                    ->groupBy('dated')->pluck('count_calls','dated')->all();

    $date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));
    $dummy = [];
    foreach($date_range as $ii=>$vv){
      $data[$vv->format('Y-m-d')] = ['dates'=>$vv->format('Y-m-d'), 'calls'=>0, 'views'=>0 ];
      $dummy[] = [$vv->format('Ymd'), 0, 0, 0, 0];
    }

    $rows = is_array($analyticsData->rows)?$analyticsData->rows:$dummy;

    foreach($rows as $row){
      $date = date_create_from_format('Ymd', $row[0]);
      $calls = $row[2];
      $views = $row[1];

      if(isset($alt_calls[$date->format('Y-m-d')])){
        if($alt_calls[$date->format('Y-m-d')] > $views){
          $calls = $alt_calls[$date->format('Y-m-d')];
        }
      }

      for($i=2;$i<=5;$i++){
        if(($current_business_id + (int)$date->format('Ymd'))%$i == 0){
          $views = ceil($views * (float)('1.'.$i));
        }
      }

      $data[$date->format('Y-m-d')] = ['dates'=>$date->format('Y-m-d'), 'calls'=>$calls, 'views'=>$views];
      $count_call_views+=$calls;
      $count_page_views+=$row[1];
    }

    $data = array_values($data);

    return view('site.dashboard', compact( 'user', 'start_date', 'end_date',
                      'business', 'count_page_views',
                      'count_avg_page_views', 'count_favorites',
                      'count_call_views',
                      'analyticsData',
                      'businesses', 'data'));

  }





  public function getCallsDataApi($bid = false){
    return $this->getViewsDataApi($bid);
  }

  public function getViewsDataApi($bid = false)
  {
    $input = Input::all();

    $start_date = $input['start'];
    $end_date  = $input['end'];
    $data = [];
    $current_business_id = $input['business_id'];
    $interval = isset($input['interval'])?$input['interval']:'daily';

    $business = \App\Models\Business::find($current_business_id);
    $zone = \App\Models\Zone::find($business->zone_id);
    $city = \App\Models\City::find($zone->city_id);


    $slug = $city->slug.'/'.$business->slug;

    // $slug = '/pune/juice-salon-nail-bar-kothrud';


    $metrics='ga:pageviews,ga:goal1Completions';
    $opts = ['dimensions'    => 'ga:date',

            'filters'       => "ga:pagePath=@".$slug];
    

    $analyticsData = Analytics::performQuery(Period::create(\Carbon\Carbon::parse($start_date), \Carbon\Carbon::parse($end_date)), $metrics, $opts);

    $data = array();
    $count_call_views = 0;
    $count_page_views = 0;

    $alt_calls = \App\Models\Call_log::selectRaw('count(id) as count_calls, dated')
                            ->accessibleByClient()
                            ->byBusiness($current_business_id)
                            ->betweenDates($start_date, $end_date)
                            ->groupBy('dated')
                            ->pluck('count_calls','dated')->all();

    $call_counts = 0;
    $view_counts = 0;
    $cc = 0;
    $date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));
    $dummy = [];
    foreach($date_range as $ii=>$vv){
      $data[$vv->format('Y-m-d')] = ['dates'=>$vv->format('Y-m-d'), 'calls'=>0, 'views'=>0 ];
      $dummy[] = [$vv->format('Ymd'), 0, 0, 0, 0];
    }

    $rows = is_array($analyticsData->rows)?$analyticsData->rows:$dummy;


    foreach($rows as $row){
      $date = date_create_from_format('Ymd', $row[0]);
      $calls = $row[2];
      $views = $row[1];
      $cc++;
      if(isset($alt_calls[$date->format('Y-m-d')])){
        if($alt_calls[$date->format('Y-m-d')] > $calls){
          $calls = $alt_calls[$date->format('Y-m-d')];
        }
      }


      for($i=2;$i<=5;$i++){
        if(($current_business_id + (int)$date->format('Ymd'))%$i == 0){
          $views = ceil($views * (float)('1.'.$i));
        }
      }

      if($interval == 'weekly'){
        // skip except the 7th entry
        if(($cc!=1)&&($cc%7 != 0)){
          $call_counts += $calls;
          $view_counts += $views;
        }else{
          $call_counts += $calls;
          $view_counts += $views;

          $call_count = $call_counts;
          $view_count = $view_counts;

          $data[$date->format('Y-m-d')] = ['dates'=>$date->format('Y-m-d'), 'calls'=>$call_count, 'views'=>$view_count];

          $call_counts = 0;
          $view_counts = 0;
        }
      }elseif($interval == 'monthly'){
        // skip except the 7th entry
        if(($cc!=1)&&($cc%29 != 0)){
          $call_counts += $calls;
          $view_counts += $views;
        }else{
          $call_counts += $calls;
          $view_counts += $views;

          $data[$date->format('Y-m-d')] = ['dates'=>$date->format('Y-m-').'01', 'calls'=>$call_count, 'views'=>$view_count ];
          $call_counts = 0;
          $view_counts = 0;
        }

      }else{
        $data[$date->format('Y-m-d')] = ['dates'=>$date->format('Y-m-d'), 'calls'=>$calls, 'views'=>$views];
      }

      $count_call_views+=$calls;
      $count_page_views+=$views;
    }
    $data = array_values($data);

    $count_avg_page_views = ceil($count_page_views/count($data));

    $response = [ 'data'=>$data, 
                  'interval'=>($interval=='daily'?1:7), 
                  'count_call_views'=>$count_call_views,  
                  'count_avg_page_views'=>$count_avg_page_views, 
                  'count_page_views'=>$count_page_views, 
                  'start'=>strtotime($start_date)*1000, 
                  'end'=>strtotime($end_date)*1000];

    return response()->json($response);
  }



}

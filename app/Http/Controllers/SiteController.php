<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator, Storage, Redirect, Response;
use Illuminate\Support\Facades\Input;

class SiteController extends Controller{
  public function clientHomeRedirector(){
    if(!\Auth::check()){
      return redirect('/unauthenticated');
    }

    $user = \App\Models\User::find(\Auth::user()->id);


    $merchant_ids = \DB::table('api_merchantuser')->where('user_id', '=', $user->id)->pluck('merchant_id','merchant_id');
    $business_ids = \App\Models\Business::whereIn('merchant_id',  $merchant_ids)->pluck('id','id')->all();

    if(count($business_ids)==0){
      return redirect('/unauthenticated');
    }


    $b_id = current($business_ids);
    mzk_client_set_default_business($b_id);
    return redirect('/'.$b_id);
  }

  public function setCurrentBusiness($id){
    mzk_client_set_default_business($id);
    return redirect()->back();
  }


  public function login(){
    $input = Input::all();
    foreach($input as $ii=>$vv){
      $input[$ii] = trim($vv); 
    }
    

    if(!isset($input['id']) || !isset($input['token'])){
      return redirect('/');
    }

    $user = \App\Models\User::find(trim($input['id']));

    if($user->access_token!=$input['token']){
      return redirect('/');
    }


    $allowed_roles = array('admin', 'sales', 'client', 'sales-admin');
    $roles = explode(',', $user->role);
    $roles_existing = array_intersect($roles, $allowed_roles);

    if(count($roles_existing)==0){
      return redirect('/');
    }

    if(isset($input['b_id'])&&(!empty($input['b_id']))){
      // we ned to check and ensure that if this is for an admin user or not
      if($user->hasTheRoles(['admin', 'sales', 'sales-admin'])){
        \Auth::loginUsingId($input['id'], true);
        mzk_client_set_default_business($input['b_id']);
        return redirect('/'.$input['b_id']);
      }elseif($user->hasTheRoles(['client'])){
        $merchant_ids = \DB::table('api_merchantuser')
                            ->where('user_id', '=', $user->id)
                            ->pluck('merchant_id','merchant_id')->all();
        $business = \App\Models\Business::find($input['b_id']);
        if($business){
          if( in_array($business->merchant_id, $merchant_ids) ){

            if(($input['id']) == '10300'){
              //dump(1);dump($input);dump($user->access_token);dump($input['token']);
            }

            \Auth::loginUsingId($input['id'], true);
            mzk_client_set_default_business($input['b_id']);
            return redirect('/'.$input['b_id']);
          }

        }else{
          return redirect('/');
        }
      }else{
        return redirect('/');
      }
    }

    $merchant_ids = \DB::table('api_merchantuser')
                        ->where('user_id', '=', $user->id)
                        ->lists('merchant_id','merchant_id');

    if(count($merchant_ids)==0){
      return redirect('/');
    }

    \Auth::loginUsingId($input['id'], true);
    return redirect('/');
  }
  public function logout(){
    \Auth::logout();
    return view('site.logout');
  }

  public function index(Request $request){

    if(!\Auth::check()){
      return view('site.404');
    }

    $dashboards = ['crm', 'content'];
    return view('site.home', compact('dashboards'));
  }

  public function contentDashboard(){
    return redirect('/content/businesses');
  }

  public function crmDashboard(){
    return redirect('/crm/campaigns');
  }

  public function changeLocale($id){
    $city = \App\Models\City::find($id);
    mzk_set_default_city($city);
    return redirect()->back();
  }

  public function uploadFile(Request $request){
    $result = array();

    $file_names = array();
    foreach($_FILES as $file_name=>$one_file){
      $photo = $request->file($file_name);

      $name = $photo->getClientOriginalName();
      $image_path = $photo->getPathName();
      $image_large = \Image::make($image_path)->resize(800, null, function ($constraint) {
        $constraint->aspectRatio();
      });

      $image_thumbnail = \Image::make($image_path)->fit(400, 275);

      $image_large = $image_large->stream();
      $image_thumbnail = $image_thumbnail->stream();

      $folder = '/images/'.str_slug($file_name).'_'.time();

      $large_path = $folder.'/large/'.$name;
      $thumb_path = $folder.'/medium/'.$name;

      \Storage::disk('s3')->put($large_path, $image_large->__toString(), 'public');
      \Storage::disk('s3')->put($thumb_path, $image_thumbnail->__toString(), 'public');
      $img = new \App\Models\Image();
      $img->description = $name;
      $img->name = $name;
      $img->image_url = 'https://s3.amazonaws.com/mazkaracdn'.$large_path;
      $img->image_thumbnail_url = 'https://s3.amazonaws.com/mazkaracdn'.$thumb_path;
      $img->save();
      $result[] = array('id'=>$img->id, 
                        'url'=>$img->image_url, 
                        'thumb'=>$img->image_thumbnail_url);
    }

    return response()->json($result);
  }


}


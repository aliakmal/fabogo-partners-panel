<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth, DB, URL, View, Redirect, Response;

use App\Models\User;
use App\Models\Business;

class LeadsController extends Controller {

	protected $business;

	public function __construct(\App\Models\Business $business){
		$this->business = $business;
	}

	public function index($bid = false)
	{
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);
    $zone = \App\Models\Zone::find($business->zone_id);
		$show_leads = false;

    $show_leads = true;

	  if($show_leads == false){
	  	return redirect('/'.$bid);
	  }

    $params = Input::all();

    $leads = \DB::table('api_lead')->select('api_lead.*')
                ->where('api_lead.city_id', '=', $zone->city_id)
                ->whereRaw('api_lead.id in 
                              (select al.lead_id 
                                from api_leadassignment al 
                                where al.business_id ="'.$business->id.'" )');

    if(isset($params['interested']) && !empty($params['interested'])){
      $leads = $leads->where('interested_in', 'like', '%'.$params['interested'].'%');
    }

    if(isset($params['search']) && !empty($params['search'])){
      $leads = $leads->where('name', 'like', '%'.$params['search'].'%')->whereOr('email', 'like', '%'.$params['search'].'%');
    }

    if(isset($params['start']) && !empty($params['start'])){
      $params['end'] = isset($params['end'])?$params['end']:\Carbon\Carbon::now()->toDateString();
      $leads = $leads->where('created_at', '>', $params['start'])->where('created_at', '<=', $params['end']);
    }

		$leads = $leads->orderBy('id', 'DESC')->paginate(20);
    $results = array();
    $current_date = false;

    foreach($leads as $lead){
      if(\Carbon\Carbon::parse($lead->created_at)->toFormattedDateString() != $current_date){
    		$current_date = \Carbon\Carbon::parse($lead->created_at)->toFormattedDateString();
    		$results[$current_date] = [];
    	}

    	$results[$current_date][] = $lead;
    }

		return view('leads.index', compact('leads', 'results', 'business', 'params'));
	}

	public function getRatingsData($bid = false){
		$input = Input::all();
		// get start and end date
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);

		$start_date = isset($input['start']) ? $input['start'] : $business->created_at;
    $end_date = isset($input['end']) ? $input['end'] : date('Y-m-d');

		$weeks = mzk_get_weeks_between_range_array($start_date, $end_date);

    $line_chart = [];
    
    for($i=0; $i<5; $i++){
	    $line_chart[$i.' to '.($i+1)] = [];
	    foreach($weeks as $vv){
		 		$line_chart[$i.' to '.($i+1)][] = [	strtotime($vv[0]), 
		 																				$this->review->query()->betweenDates($vv[0], $vv[1])
		 																										->byBusiness($current_business_id)
		 																										->betweenRatings($i, ($i+1))->count()]; 
	    }
    }

		return response()->json($line_chart);
	}


	public function create()
	{
		return view('reviews.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['user_id'] = \Auth::user()->id;
		$validation = Validator::make($input, \App\Models\Review::$rules);

		if ($validation->passes())
		{
			$review = $this->review->create($input);
      $data = array('user_id' =>  $input['user_id'], 
                    'verb'  =>  (trim($input['body'])!=""?'reviewed':'rated'), 
                    'itemable_type' =>  'Review', 
                    'itemable_id' =>  $review->id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $input['user_id'];
      $feed->verb = 'reviewed'; 
      $feed->itemable_type = 'Review';
      $feed->itemable_id = $review->id;
      $feed->save();

			return redirect()->back();//route('businesses.show', array('id'=>$input['business_id']));
		}

		return redirect()->back()//route('businesses.show', array('id'=>$input['business_id']))
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	public function show($bid = false, $id)
	{
		$review = $this->review->findOrFail($id);
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);


		return view('reviews.show', compact('review', 'business'));

	}

	public function edit($id)
	{
		$review = $this->review->find($id);

		if (is_null($review))
		{
			return redirect()->route('reviews.index');
		}

		return view('reviews.edit', compact('review'));
	}

	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$input['user_id'] = \Auth::user()->id;

		$validation = Validator::make($input, Review::$rules);

		if ($validation->passes())
		{
			$review = $this->review->find($id);
			$review->update($input);

			return Redirect::back();//('reviews.show', $id);
		}
		return Redirect::back();

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($bid, $id){

		$review = $this->review->find($id);
    $business = $review->business;
    $review->delete();
    $business->updateRatingAndReviewsCount();

		return Redirect::back();//('reviews.index');
	}

}

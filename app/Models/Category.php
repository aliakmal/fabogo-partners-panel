<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Eloquent{
  protected $guarded = array();
  use Sluggable;

  /**
   * Return the sluggable configuration array for this model.
   *
   * @return array
   */
  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }

  public function __construct(){
    parent::__construct();
    $this->table = 'api_category';
  }

  public static $rules = ['description'=>'required',
                          'name'=>'required',
                          'active'=>'required'];

  public $fields = array('description','name','slug','active','business_count','old_id');

  public $fillables = array('description','name','slug','active','business_count','old_id');

  public function isActive(){
    return $this->active == 1 ? true : false;
  }

}

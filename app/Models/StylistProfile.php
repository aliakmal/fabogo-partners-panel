<?php
namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class StylistProfile extends Eloquent {
	protected $guarded = array();


  public function __construct(array $attributes = array()) {
    $this->table = 'api_stylistprofile';

    // IMPORTANT:  the call to the parent constructor method
    // should always come after we define our attachments.
    parent::__construct($attributes);
  }

  public function resume(){
    return $this->belongsTo('\App\Models\Resume');
  }
}

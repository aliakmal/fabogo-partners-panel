<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Business_service extends Eloquent{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_businessservice';
  }

  public static $fields = array('description', 'name', 'price', 'lowest_price', 'discount_percentage', 'discounted_price', 'final_price', 'gender', 'duration', 'active', 'visibility', 'old_id', 'business_id', 'service_id');

  protected $fillable = array('description', 'name', 'price', 'lowest_price', 'discount_percentage', 'discounted_price', 'final_price', 'gender', 'duration', 'active', 'visibility', 'old_id', 'business_id', 'service_id');

  public function service(){
    return $this->belongsToMany('App\Models\Service');
  }

  public function business(){
    return $this->belongsTo('App\Models\Business'); 
  }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Cviebrock\EloquentSluggable\Sluggable;

class Service extends Eloquent{
  protected $guarded = array();
  use Sluggable;

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }

  public function __construct(){
    parent::__construct();
    $this->table = 'api_service';
  }

  public static $rules = [
  'description'=>'required',
  'name'=>'required',
  'active'=>'required',
  ];

  public $fields = array('description','parent_id','name','slug','gender','business_count','active','old_id','female_image_id','icon_image_id','male_image_id');

  public $fillables = array('description','parent_id','name','slug','gender','business_count','active','old_id','female_image_id','icon_image_id','male_image_id');

  public function categories(){
    return $this->belongsToMany('App\Models\Category', 'api_category_services', 'service_id', 'category_id'); 
  }
  public function isActive(){
    return $this->active == 1 ? true : false;
  }
  public function businesses(){
    return $this->belongsToMany('App\Models\Business', 'api_businessservice', 'service_id', 'business_id')
                    ->withPivot('description','name','price','lowest_price','discount_percentage','discounted_price','final_price','gender','duration','active','visibility'); 
  }

}

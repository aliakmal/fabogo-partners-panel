<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

use Codesleeve\Stapler\Style;
use Codesleeve\Stapler\File\Image\Resizer;
use \Imagine\Image\Box;
use \Imagine\Image\Point;


class Upload extends Eloquent {
// implements StaplerableInterface

  protected $guarded = array();

  public function __construct(array $attributes = array()) {
    $styles = [
          'thumbnail' => ['dimensions' => '100x100#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
          'bigThumbnail' => ['dimensions' => '400x400#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
          'small' => ['dimensions' => '175x175#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
          'large' => function($file, $imagine) {
              $watermark = $imagine->open(storage_path().'/app/assets/images/watermark-200.png');
              $watermarkSize = $watermark->getSize();
              $image     = $imagine->open($file->getRealPath());

              $dir = storage_path().'/media/photos_watermarked_'.md5(time());
              mkdir($dir);
              $filename = $dir.'/'.$file->getFilename();
              $size = $image->getSize()->widen(800);
              $image->resize($size)->save($filename);                                 // Get the size of the uploaded image.
              $image     = $imagine->open($filename);              // Create an instance of ImageInterface for the uploaded image.
              $size = $image->getSize();

              $bottomRight = new \Imagine\Image\Point(($size->getWidth() - $watermarkSize->getWidth())/2, ($size->getHeight() - $watermarkSize->getHeight())/2);
              $image->paste($watermark, $bottomRight);
              return $image;
          }
    ];

    $this->hasAttachedFile('image', [
        'styles' => $styles,
        'storage' => 's3',
        's3_client_config' => [
            'key' => 'AKIAI37S25ETHXHU7YMQ',
            'secret' => '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',
            'region' => 'us-east-1',
            'version'=> 'latest',
            'credentials'=>[
              'key' => 'AKIAI37S25ETHXHU7YMQ',
              'secret' => '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',
            ]
        ],
        's3_object_config' => [
            'Bucket' => 'mazkaracdn'
        ],
        'default_url' => '/defaults/:style/missing.png',
        'keep_old_files' => true        
    ]);


        parent::__construct($attributes);
    }


}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Review extends Eloquent {
  //use GetStream\StreamLaravel\Eloquent\ActivityTrait;
	protected $guarded = array();
    protected $morphClass = 'Review';


  public function __construct(){
    parent::__construct();
    $this->table = 'api_review';
  }

  const PROFANE = 'profane';
  const PENDING = 'pending';
	
  public static $rules = array(
		'rating' => 'required',
		//'body' => 'required',
		'user_id' => 'required',
		'business_id' => 'required'
	);

  public static $update_rules = array(
    'rating' => 'required',
    //'body' => 'required',
    //'user_id' => 'required',
    //'business_id' => 'required'
  );


  public static $review_rules = array(
    'rating' => 'required',
    'body' => 'required',
    'user_id' => 'required',
    'business_id' => 'required'
  );

  public static $rating_rules = array(
    'rating' => 'required',
    'business_id' => 'required'
  );


  public function activityExtraData(){
    return array('review'=>$this->toArray(), 'business'=>$this->business->toArray());
  }

  public function comments(){
    return $this->morphMany('App\Models\Comment', 'commentable');
  }

  public function scopeBetweenRatings($query, $start, $end){
    return $query->where('rating', '>', $start)->where('rating', '<=', $end);
  }

  public function scopeDated($query, $dt){
    return $query->where(DB::raw('DATE(created_at)'), $dt);
  }

  public function scopeBetweenDates($query, $start, $end){
    return $query->where('reviews.created_at', '>', $start)->where('reviews.created_at', '<=', $end);
  }

  public function scopeOnlyActive($query){
    return $query->where('status', '=', 'active');
  }

  public function scopeIsCheatRate($query){
    return $query->where('is_cheat', '>', 0);
  }

  public function scopeIsNotCheatRate($query){
    return $query->where('is_cheat', '=', 0);
  }

  public function isActive(){
    return $this->status == 'active' ? true : false;
  }

  public static function ratingOptions(){
    $o = array('1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0');
    $a = [];
    foreach($o as $i=>$v){
      $a[$v] = $v;
    }

    return $a;
  }

  public static function ratingOptionsMain(){
    $o = array('1.0', '2.0', '3.0', '4.0', '5.0');
    $a = [];
    foreach($o as $i=>$v){
      $a[$v] = $v;
    }

    return $a;
  }


  private static $badWords = ['fuck', 'bitch'];

  private function isReviewClean(){
   foreach(self::$badWords as $word){        
      if(strstr($this->body, $word))        
      {       
        $this->flags = self::PROFANE;
        return;
      }       
    }       
    $this->flags = $this->flags!=self::PENDING ? '' : $this->flags;
  }

  public function afterCreate() {
    $this->updateBusinessReviews();
    if($this->user){
      $this->user->updateRatingsCount();
      $this->user->updateReviewsCount();
    }
  }

  public function flagPending(){
    $this->flags = self::PENDING;

    $this->save();
  }

  public function flagClear(){
    $this->flags = '';
    $this->save();
  }

  public function afterSave(){
    $this->updateBusinessReviews();
  }

  public function beforeSave(){
    $this->isReviewClean();
    $this->setForeignDefaults();
  }

  function setForeignDefaults(){
    $this->business_id = $this->business_id == 0 ? null : $this->business_id; 
  }

  public function beforeCreate(){
    $this->isReviewClean();
  }


  public function beforeDelete(){
    //$this->decrementBusinessReviewsCount();
  }
  
  public function updateBusinessReviews(){
    $this->business->updateRatingAndReviewsCount();
  }
  
  public function updateBusinessReviewsCount(){
    $this->business->updateReviewCount();
    $this->business->updateRatingsCount();
  }

  public function updateBusinessRatingsAverage(){
    $this->business->updateAverageRating();
  }


  public function decrementBusinessReviewsCount(){
    $this->business()->decrement('reviews_count');
  }


  public function getAuthorNameAttribute(){
    return $this->user_id > 0 ? $this->user->full_name : 'User';
  }
  public function user(){
    return $this->belongsTo('App\Models\User', 'user_id');
  }

  public function business(){
    return $this->belongsTo('App\Models\Business', 'business_id');
  }
  public function businessMinimal(){
    return $this->belongsTo('App\Models\Business', 'business_id')->select('id', 'name');
  }

  public function scopeByBusiness($query, $b_id){
    return $query->where('business_id', '=', $b_id);//->Where('body', '<>', ' ');
  }

  public function scopeByUser($query, $user_id){
    return $query->where('user_id', '=', $user_id);//->Where('body', '<>', ' ');
  }

  public function scopeByIP($query, $ip){
    return $query->where('ip', '=', $ip);//->Where('body', '<>', ' ');
  }
  
  public function scopeByFlags($query, $flag){
    return $query->where('flags', '=', $flag);//->Where('body', '<>', ' ');
  }

  public function scopeByRating($query, $rating){
    return $query->where('rating', '=', $rating);
  }



  public function scopeIsCompleteReview($query){
    return $query->where('user_id', '>', '0')->Where('body', '<>', ' ');
  }

  public function scopeIsComplete($query){
    return $query->where('user_id', '>', '0')->Where('body', '<>', ' ');
  }


  public function scopeIsIncomplete($query){
    return $query->where('body', '=', ' ');//->orWhere('body', '=', '');
  }

  public function scopeIsReview($query){
    return $query->where('user_id', '>', '0')->Where('body', '<>', ' ');
  }

  public function scopeIsRating($query){
    return $query->where('body', '=', ' '); //where('user_id', '<>', '0')->orW
  }

  public function isARating(){
    return $this->body == ' ' || $this->body == '' ?true:false; //where('user_id', '<>', '0')->orW
  }

  public function scopeByLocale($query, $locale = false){

    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->join('businesses', 'reviews.business_id', '=', 'businesses.id')
        ->where('businesses.city_id', '=', $locale);
  }


  public function shares(){
    return $this->morphMany('App\Models\Share', 'sharable');
  }

  public function incrementViewCount(){
    $c = Counter::select()->byCountable('Review', $this->id)->byType(Counter::PAGE_VIEWS)->today()->get();
    if($c->count()==0){
      $c = Counter::create(['countable_type'=>'Review', 'countable_id'=>$this->id, 'type'=>Counter::PAGE_VIEWS, 'dated'=>date('Y-m-d'), 'views'=>1]);
    }

    $c->first()->incrementViewCount();

  }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


class Module_access extends Eloquent
{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_businessmoduleaccess';
  }

  public static $fields = array( 'description','access','business_id','module_id');
  protected $fillable = array( 'description','access','business_id','module_id');

  public function business(){
    return $this->belongsTo('App\Models\Business');
  }

  public function module(){
    return $this->belongsTo('App\Models\Module');
  }

}
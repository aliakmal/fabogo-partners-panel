<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent, Auth;


use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;

class Call_log extends Eloquent{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_calllog';
  }

  public static $rules = [];

  public $fields = array('description', 'dated', 'timed', 'called_number', 'caller_number', 'caller_duration', 'agent_list', 'call_connected_to', 'call_transfer_status', 'call_transfer_duration', 'call_recording_url', 'call_start_time', 'call_pickup_time', 'caller_circle', 'business_id', 'is_downloaded', 'is_lead', 'state', 'is_ppl', 'media_file_name', 'media_file_size', 'media_content_type', 'media_updated_at', 'invoice_item_id', 'type_of_call');

  public $fillables = array('description', 'dated', 'timed', 'called_number', 'caller_number', 'caller_duration', 'agent_list', 'call_connected_to', 'call_transfer_status', 'call_transfer_duration', 'call_recording_url', 'call_start_time', 'call_pickup_time', 'caller_circle', 'business_id', 'is_downloaded', 'is_lead', 'state', 'is_ppl', 'media_file_name', 'media_file_size', 'media_content_type', 'media_updated_at', 'invoice_item_id', 'type_of_call');

  const STATE_CLEAR = 'clear';
  const STATE_UNBILLED = 'unbilled';
  const STATE_DISPUTED = 'disputed';
  const STATE_BILLED = 'billed';
  const STATE_RESOLVED = 'resolved';

  public function scopeOnlyDownloaded($query){
    return $query->where('is_downloaded', '=', 1);
  }

  public function scopeAccessibleByClient($query){
    return $query->whereIn('type_of_call', [0,3]);
  }

  public function isAccessibleByClient(){
    if(in_array($this->type_of_call, [0,3])){
      return true;
    }

    return false;
  }

  public function scopeOnlyNotDownloaded($query){
    return $query->where('is_downloaded', '<>', 1);
  }


  public function scopeWithUrl($query){
    return $query->whereNotIn('call_recording_url', ['None', ' ', '']);
  }

  public function scopeHoursAgo($query, $hours = 3){
    $now = \Carbon\Carbon::now();

    $now->subHours($hours);                  //

    return $query->where('created_at', '<', (date('Y-m-d H:i', strtotime($now))));
  }



  public function scopeByBusiness($query, $business_id){
    return $query->where('business_id', '=', $business_id);
  }

  public function scopeByBusinesses($query, $business_ids){
    return $query->whereIn('business_id', $business_ids);
  }

  public function scopeBillable($query){
    return $query->whereIn('state', [self::STATE_UNBILLED, self::STATE_RESOLVED]);
  }

  public function scopeOnlyConnected($query){
    return $query->where('call_transfer_status', '=', 'Connected');
  }
  public function isConnected(){
    return $this->call_transfer_status == 'Connected' ? true:false;
  }

  public function scopeBetweenDates($query, $start, $end){
    return $query->where('dated', '>', $start)->where('dated', '<=', $end);
  }

  public function beforeCreate(){
    $this->setInitialStatus();
  }

  public function dispute(){
    $this->state = self::STATE_DISPUTED;
    $this->save();
  }

  public function resolve(){
    $this->state = self::STATE_RESOLVED;
    $this->is_lead = 1;
    $this->save();
  }

  public function resetLead(){
    $this->state = self::STATE_CLEAR;
    $this->is_lead = 0;
    $this->save();
  }

  public function makeClearToBillable(){
    $this->state = self::STATE_UNBILLED;
    $this->is_lead = 1;
    $this->save();
  }

  public function comments(){
    //return $this->morphMany('Comment', 'commentable');
  }

  public function getDurationAttribute(){
    if(!strstr($this->caller_duration, ':')){
      return $this->caller_duration>0?$this->caller_duration:0;
    }else{
      sscanf($this->caller_duration, "%d:%d:%d", $hours, $minutes, $seconds);

      $time_seconds = isset($seconds) ? ($hours * 3600) + ($minutes * 60) + $seconds : ($hours * 60) + $minutes;
      return $time_seconds;
    }
  }


  public function isDisputable(){
    
    if($this->isLead()){
      if($this->state!=self::STATE_DISPUTED){
        return true;
      }
    }

    return false;
  }

  public function isClear(){

    if($this->state==self::STATE_CLEAR){
      return true;
    }

    return false;
  }

  public function isResolvable(){

    if($this->isLead()){

      if($this->state==self::STATE_DISPUTED){
        return true;
      }
    }

    return false;
  }
  public function isResetable(){
  
    if(in_array($this->state, [self::STATE_UNBILLED, self::STATE_DISPUTED, self::STATE_RESOLVED])){
      return true;
    }else{
      return false;
    }
  }

  public function canBeResolvedByUser($user = false){
    $user = $user ? $user : Auth::user();
    if($user->hasTheRoles(['admin','moderator','sales-admin','sales'])){
      return true;
    }else{
      return false;
    }
  }

  public function isPotentialLead(){
    return true;
    if($this->duration >= 30){
      return true;
    }else{
      return false;
    }
  }

  public function isLead(){
    if($this->is_lead == 1){
      return true;
    }else{
      return false;
    }
  }
  
  function isPPL(){
    return $this->is_ppl == 1 ? true:false;
  }

  public function setInitialStatus(){
    if($this->isPotentialLead()){
      $this->is_lead = 1;
      $this->state = self::STATE_UNBILLED;
    }else{
      $this->is_lead = 0;
      $this->state = self::STATE_CLEAR;
    }
  }



}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Cviebrock\EloquentSluggable\Sluggable;


class Resume extends Eloquent{
	protected $guarded = array();
  protected $morphClass = 'Resume';



  public static $fields = array('name', 'location',
                                'nationality','uploaded_by', 
                                'city_id', 'phone', 'experience', 
                                'salary');

  public function __construct(array $attributes = array()) {
    $this->table = 'api_resume';

    // IMPORTANT:  the call to the parent constructor method
    // should always come after we define our attachments.
    parent::__construct($attributes);
  }

  function deleteBasic(){
    $sql ='delete from resumes where id = '.$this->id;

    $d = \DB::delete($sql);
  }

  public function stylist_profile(){
    return $this->hasOne('\App\Models\StylistProfile');
  }


  function getMediaMatter(){
    $url = $this->media->url();
    return strstr($url, 'missing.png')?'none':$url;
  }

  function getSpecializations(){
    $specializations = array('Spa Manager','Masseuse','Masseur','Spa Therapists',
      'Spa Receptionist','Spa Advisor','Salon Manager','Salon Receptionist',
      'Hair Stylist','Esthetician','Makeup Artist','Nail Technician',
      'Beautician','Beauty Therapist','Beauty Advisor');
    asort($specializations);
    $data = array();
    foreach($specializations as $vv){
      $data[str_slug($vv)] = $vv;
    }

    return $data;
  }

  function hasCVAttached(){
    return $this->doc_file_name == '' ? false : true;
  }


  function getExperiences(){
    $experiences = array(
      'No Experience',
      '6 months or less',
      '6 to 12 months',
      '1 to 2 years',
      '2 to 5 years',
      '5 years and above');
    $data = array();
    foreach($experiences as $vv){
      $data[str_slug($vv)] = $vv;
    }

    return $data;
  }



  public function specializations(){
    return $this->hasMany('\App\Models\Specialization');
  }

  public function businesses(){
    return $this->belongsToMany('\App\Models\Business')->withPivot('allocated_at');;
  }

  public function scopeBetweenDates($query, $start, $end){
    return $query->where('created_at', '>', $start)->where('created_at', '<=', $end);
  }

  public function scopeByBusiness($query, $bid){
    return $query->whereHas('businesses', function ($query) use ($bid) {
                        $query->where('businesses.id', '=', $bid);
                });  
  }

  public function scopeBySpecializations($query, $specializations){
    return $query->whereHas('specializations', function ($query) use ($specializations) {
              $query->whereIn('specializations.specialization', $specializations);
            });  
  }

  public function city_name(){
    return $this->city_id > 0 ? $this->city->name : '';
  }

  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('api_stylistprofile.city_id', '=', $locale)->join('api_stylistprofile', 'api_stylistprofile.resume_id', '=', 'api_resume.id');
  }

  public function scopeByPhone($query, $phone = false){
    return $query->where('api_stylistprofile.phone_number', 'like', $phone.'%')
                        ->join('api_stylistprofile', 'api_stylistprofile.resume_id', '=', 'api_resume.id');
  }

  public function scopeByEmail($query, $email = false){
    return $query->where('api_stylistprofile.email_address', 'like', $email.'%')
                        ->join('api_stylistprofile', 'api_stylistprofile.resume_id', '=', 'api_resume.id');
  }

  public function scopeSearchAll($query, $search = ''){
    return $query->whereRaw('((api_stylistprofile.stylist_name like "%'.$search.'%") || 
                      (api_resume.current_location like "%'.$search.'%") || 
                      (api_resume.experience like "%'.$search.'%") || 
                      (api_resume.nationality like "%'.$search.'%"))');
  }

  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }

  public function getNameAttribute(){
    return $this->stylist_profile ? $this->stylist_profile->stylist_name: '';
  }

  public function getNationalityAttribute(){
    return $this->stylist_profile ? $this->stylist_profile->nationality: '';
  }

  public function scopeByNationality($query, $nationality){
    return $query->where('api_stylistprofile.nationality', 'like', $nationality.'%')
                        ->join('api_stylistprofile', 'api_stylistprofile.resume_id', '=', 'api_resume.id');
  }

  public function getLocationAttribute(){
    return $this->current_location;
  }

  public function getEmailAddressAttribute(){
    return $this->stylist_profile ? $this->stylist_profile->email: ''; 
  }

  public function getPhoneAttribute(){
    return $this->stylist_profile ? $this->stylist_profile->phone_number: ''; 
  }





}

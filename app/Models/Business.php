<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Cviebrock\EloquentSluggable\Sluggable;


class Business extends Eloquent
{
  protected $guarded = array();

  public function __construct(){
    parent::__construct();
    $this->table = 'api_business';
  }

  use Sluggable;

  protected $dispatchesEvents = [
      'creating' => \App\Events\BusinessUpdated::class,
      'updating' => \App\Events\BusinessUpdated::class,
  ];

  /**
   * Return the sluggable configuration array for this model.
   *
   * @return array
   */
  public function sluggable()
  {
      return [
          'slug' => [
              'source' => ['name', 'zone.name']
          ]
      ];
  }

  public static $rules = [];

  public static function getTypes(){
    return ['business'=>'Business - with location', 'home-service'=>'Home Service Based Business', 'freelancer'=>'Freelancer - No Location'];
  }
  public static function getCostOptions(){
    return [ 0=>'Not set', 1=>'Economical', 2=>'Premium', 3=>'Luxury']; //, 4=>'Fabuxpensive'
  }


  public static $fields = array( 'description','name','phone_number', 'phone', 'email','city_name','zone_cache',
                          'address','active','slug','gender_spec','latitude','longitude',
                          'rating_average','is_home_service','is_featured','share_link',
                          'old_id','chain_id','merchant_id','zone_id','display_json',
                          'popularity','reviews_count','active_packages_count',
                          'total_ratings_count');


  public static $fillables = array( 'description','name','phone_number','email','city_name','zone_cache',
                          'address','active','slug','gender_spec','latitude','longitude',
                          'rating_average','is_home_service','is_featured','share_link',
                          'old_id','chain_id','merchant_id','zone_id','display_json',
                          'popularity','reviews_count','active_packages_count',
                          'total_ratings_count');
  
  public function isFeatured(){
    return $this->is_featured == 0 ? false : true;
  }

  public function hasChain(){
    return $this->chain_id > 0 ? true : false;
  }


  public function module_accesses(){
    return $this->hasMany('App\Models\Module_access');
  }

  public function modules(){
    return $this->hasManyThrough('App\Models\Module', 'App\Models\Module_access');
  }

  public function getDisplayableAttribute(){
    return $this->id.' - '.$this->name.', '.$this->zone_cache.', '.$this->city_name;
  }

  public function group(){
    return $this->belongsTo('\App\Models\Group', 'chain_id');
  }

  public function getWebsiteAttribute(){
    return $this->details?$this->details->website:null;
  }

  public function getFacebookAttribute(){
    return $this->details?$this->details->facebook:null;
  }

  public function getTwitterAttribute(){
    return $this->details?$this->details->twitter:null;
  }

  public function getInstagramAttribute(){
    return $this->details?$this->details->instagram:null;
  }

  public function getGoogleAttribute(){
    return $this->details?$this->details->google:null;
  }

  public function getCostEstimateAttribute(){
    return $this->details?$this->details->cost_estimate:null;
  }

  public function getTypeAttribute(){
    return $this->details?$this->details->type:null;
  }

  public function getPreferredEmailAttribute(){
    return $this->details?$this->details->preferred_email:null;
  }

  public function getPreferredPhoneAttribute(){
    return $this->details?$this->details->preferred_phone:null;
  }

  public function getAboutAttribute(){
    return $this->details?$this->details->about:null;
  }

  public function thePreferredEmail(){
    if(!empty($this->preferred_email)){
      return $this->preferred_email;
    }

    if($this->merchant){
      return $this->merchant->email;
    }

    return null;
  }

  public function thePreferredPhone(){
    if(!empty($this->preferred_phone)){
      return $this->preferred_phone;
    }

    if($this->merchant){
      return $this->merchant->phone;
    }

    return null;
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return  $query->whereHas('zone', function ($q) use ($locale) {
        $q->where('city_id', '=', $locale);
    });
  }

  public function getLiveUrl(){
    $str = 'https://www.fabogo.com/';
    $str.=$this->getCitySlug().'/'.$this->slug;
    return $str;
  }

  public function getCurrency(){
    return $this->zone->city?$this->zone->city->currency:'';
  }

  public function getCitySlug(){
    return $this->zone->city?$this->zone->city->slug:'';
  }

  public function hasOldId(){
    return $this->old_id > 0 ? true : false;
  }

  public function isCurrentlyAllocatedAVirtualNumber(){
    return  $this->allocated_a_current_virtual_number() ? true : false;
  }

  public function uploadPhotos($images, $type){

    foreach ($images as $photo) {

      $name = $photo->getClientOriginalName();
      $image_path = $photo->getPathName();
      $image_large = \Image::make($image_path)->resize(800, null, function ($constraint) {
        $constraint->aspectRatio();
      })->encode('jpg', 30);

      $image_thumbnail = \Image::make($image_path)->fit(400, 275)->encode('jpg', 30);

      $image_large = $image_large->stream();
      $image_thumbnail = $image_thumbnail->stream();

      $folder = '/images/'.$this->id.'_'.time();

      $large_path = $folder.'/large/'.$name;
      $thumb_path = $folder.'/medium/'.$name;

      \Storage::disk('s3')->put($large_path, $image_large->__toString(), 'public');
      \Storage::disk('s3')->put($thumb_path, $image_thumbnail->__toString(), 'public');

      $this->photos()->create(array(
        'description'=>$type.' image for business '.$this->id,
        'name'=>$name,
        'image_url'=>'https://s3.amazonaws.com/mazkaracdn'.$large_path,
        'image_thumbnail_url'=>'https://s3.amazonaws.com/mazkaracdn'.$thumb_path,
        'imagable_id'=>$this->id,
        'image_type'=>$type,
        'image_file_size'=>0,
        'business_id'=>$this->id,
        'slot'=>0
      ));
    }

  }

  public function scopeSearchBasic($query, $search){
    return $query->whereRaw('(name like "%'.$search.'%")');
  }
  
  public function scopeOfZones($query, $zones){
    return $query->whereIn('zone_id', $zones);
  }

  public function scopeOfChains($query, $chains){
    return $query->whereIn('chain_id', $chains);
  }

  public function currently_allocated_number(){
    $num = '';
    if($this->isCurrentlyAllocatedAVirtualNumber()){
      $num = $this->current_virtual_number_allocation()->body;
    }

    return $num;
  }

  public function virtual_number_allocations(){
    return $this->hasMany('App\Models\Virtual_number_allocation');
  }


  public function virtual_number_allocations_current(){
    return $this->hasMany('App\Models\Virtual_number_allocation')->whereIn('api_virtualnumberallocation.state',['active', 'hold'])->get();;
  }


  public function allocated_a_current_virtual_number(){
    $r = $this->virtual_number_allocations()->whereIn('api_virtualnumberallocation.state',['active', 'hold'])->get();

    if(count($r)>0){
      return $r->first();
    }

    return false;
  }


  public function current_virtual_number_allocation(){
    $r = $this->virtual_number_allocations()->whereIn('api_virtualnumberallocation.state',['active', 'hold'])->get();
    if(count($r)>0){
      return $r->first();
    }

    return false;
  }

  public function current_held_virtual_number_allocation(){

    if($this->virtual_number_allocations()->where('api_virtualnumberallocation.state', '=', 'hold')->count()>0){
      return $this->virtual_number_allocations()->where('api_virtualnumberallocation.state', '=', 'hold')->first();
    }

    return false;
  }

  public function current_virtual_number_allocation_number(){
    $c = $this->current_virtual_number_allocation();
    if($c){
      if($c->virtual_number()->count()>0){
        return $c->virtual_number()->first()->body;
      }
    }

    return false;
  }

  public function current_virtual_number_allocation_id(){
    $allocation = $this->current_virtual_number_allocation();

    if($allocation!=false){
      return $allocation->id;
    }

    return false;
  }

  public function categories(){
    return $this->belongsToMany('App\Models\Category', 'api_business_categories', 'business_id', 'category_id'); 
  }

  public function highlights(){
    return $this->belongsToMany('App\Models\Highlight', 'api_business_highlights', 'business_id', 'highlights_id'); 
  }

  public function services(){
    return $this->belongsToMany('App\Models\Service', 'api_businessservice', 'business_id', 'service_id')
                ->withPivot('description','name','price','lowest_price','discount_percentage','discounted_price','final_price','gender','duration','active','visibility', 'id'); 
  }


  public function getRichnessColor(){
    $css = 'default';

    if($this->popularity > 0){
    $css = 'danger';

      if($this->popularity > 4){
        $css = 'warning';
      }

      if($this->popularity > 9){
        $css = 'success';
      }
    }

    return $css;
  }

  public function scopeAllocatableToMerchants($query){
    return $query->whereRaw('((api_business.merchant_id = 0) OR (api_business.merchant_id is null))');
  }

  public function merchant(){
    return $this->belongsTo('\App\Models\Merchant', 'merchant_id');
  }

  public static function getStockPhotos(){
    $p = [];
    for($i=1;$i<=78;$i++){
      $p[] = $i;
    }
    return $p;
  }

  public function getCoverImageUrl(){
    if($this->cover()->first()){
      return $this->cover()->first()->image_url;
    }

    return null;
  }




  public function setCoverImage($cover_image){
    $cover_image_url = mzk_assets('assets/stock/'.$cover_image.'.jpg');
    $this->photos()->update(['image_type'=>'venue-image']);

    $this->photos()->create(array(
      'description'=>'cover image for business '.$this->id,
      'name'=>'cover-image-'.$this->id,
      'image_url'=>$cover_image_url,
      'image_thumbnail_url'=>$cover_image_url,
      'imagable_id'=>$this->id,
      'image_type'=>'cover-image',
      'image_file_size'=>0,
      'business_id'=>$this->id,
      'slot'=>0
    ));
  }

  public function details(){
    return $this->hasOne('\App\Models\Business_details', 'business_id');
  }

  public function zone(){
    return $this->belongsTo('\App\Models\Zone', 'zone_id');
  }

  public function photos(){
    return $this->hasMany('\App\Models\Image')->whereIn('image_type', ['venue-image', 'cover-image']);
  }

  public function cover(){
    return $this->hasOne('\App\Models\Image')->whereIn('image_type', ['cover-image']);
  }


  public function rateCards(){
    return $this->hasMany('\App\Models\Image')->where('image_type', '=', 'rate-card');
  }

  public function timings() {
    return $this->hasMany('App\Models\Timing')->orderby('open', 'asc'); 
  }

  public function removeTimings($ids){
    if(!is_array($ids)){
      $ids = [$ids];
    }

    foreach($ids as $id){
      $t = \App\Models\Timing::findOrFail($id);
      $t->delete($id);
    }
  }

  public function saveTimings($timings){
    if(!is_array($timings)){
      return false;
    }
    foreach($timings as $timing):

      if(isset($timing['id']) && ($timing['id'] > 0)){ // if we have an id so consider this is an update request

        $t = \App\Models\Timing::find($timing['id']);

        // if this has been marked for deletion 
        if($t):
          if(isset($timing['deletable']) && ($timing['deletable'] > 0)){
            $t->delete($timing['id']);  // delete it
          }else{              // else
            unset($timing['deletable']);
            $t->update($timing);  // update it
          }
        endif;
      }else{
        unset($timing['deletable']);
        $t = new \App\Models\Timing();
        foreach($timing as $ii=>$vv){
          $t->$ii=$vv;
        }

        $this->timings()->save($t); // save a new timing
      }
    endforeach;


  } 


  public function getPhoneNumberAttribute($value){
    if(is_array(json_decode($value))){
      return json_decode($value);
    }else{
      return [$value];
    }
  }

  public function setPhoneNumberAttribute($value){
    $val = [];
    foreach($value as $v){
      if(trim($v)!='')
        $val[] = $v;
    }
    $this->attributes['phone_number'] = json_encode($val);
  }

  public function getPhoneAttribute($value){
    if(is_array(json_decode($value))){
      return json_decode($value);
    }else{
      return [$value];
    }
  }

  public function setPhoneAttribute($value){
    $val = [];
    foreach($value as $v){
      if(trim($v)!='')
        $val[] = $v;
    }
    $this->attributes['phone_number'] = json_encode($val);
  }

  public function getEmailAttribute($value){
    if(is_array(json_decode($value))){
      return json_decode($value);
    }else{
      return [$value];
    }
  }

  public function setEmailAttribute($value){
    $val = [];
    foreach($value as $v){
      if(trim($v)!='')
        $val[] = $v;
    }
    $this->attributes['email'] = json_encode($val);
  }
  public function isUAE(){
    if($this->zone){
      return ($this->zone->city_id == 1 ? true:false);
    }

    return false;
  }

}

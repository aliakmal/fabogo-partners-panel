<?php
namespace Task;

use Mage\Task\AbstractTask;

class Permissions extends AbstractTask
{
  public function getName(){
    return 'Fixing file permissions';
  }

  public function run(){
    $env = $this->getParameter('env', 'production'); 
    $folders = array(
                      'production'=>'partners.fabogo.com',
                    );
    $folder = $folders[$env];
    echo "Fixing the file permissions on /var/www/".$folder."/current/app ...";
    $command = 'cd /var/www/'.$folder.'/current; sudo chmod 777 storage/ -R; sudo chmod 777 bootstrap/cache/ -R;';

    $result = $this->runCommandRemote($command);

    return $result;
  }
}
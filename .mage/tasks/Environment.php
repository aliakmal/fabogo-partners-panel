<?php
namespace Task;

use Mage\Task\AbstractTask;

class Environment extends AbstractTask
{
  public function getName(){
    return 'Setting Environment';
  }

  public function run(){
    $env = $this->getParameter('env', 'production'); 
    $folders = array(
                      'production'=>'partners.fabogo.com',
                    );
    $folder = $folders[$env];
    echo "Copying the environment variables from .env.".$env.' to .env ... ';
    $command = 'rm /var/www/'.$folder.'/current/.env; cp /var/www/'.$folder.'/current/.env.'.$env.' /var/www/'.$folder.'/current/.env;';
    $result = $this->runCommandRemote($command);

    return $result;
  }
}